import Map from 'ol/Map';
import View from 'ol/View';
import Source from 'ol/source/Source';
import OSMSource from 'ol/source/OSM';
import TileDebugSource from 'ol/source/TileDebug';
import VectorSource from 'ol/source/Vector';
import XYZSource from 'ol/source/XYZ';
import TileWMSSource from 'ol/source/TileWMS';
import WMTSSource from 'ol/source/WMTS';
import ImageWMSSource from 'ol/source/ImageWMS';
import WMTSTileGrid from 'ol/tilegrid/WMTS';
import Layer from 'ol/layer/Layer';
import BaseLayer from 'ol/layer/Base';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import ImageLayer from 'ol/layer/Image';
import Feature from 'ol/Feature';
import Overlay from 'ol/Overlay';
import Interaction from 'ol/interaction/Interaction';
import { Circle as CircleStyle, Fill, Stroke, Icon, Style, Text } from 'ol/style';
import LineString from 'ol/geom/LineString';
import Polygon from 'ol/geom/Polygon';
import Circle from 'ol/geom/Circle';
import Point from 'ol/geom/Point';
import MultiPoint from 'ol/geom/MultiPoint';
import Draw from 'ol/interaction/Draw';
import BaseEvent from 'ol/events/Event';
import GeometryType from 'ol/geom/GeometryType';
import OverlayPositioning from 'ol/OverlayPositioning';
import { Zoom, Rotate, Attribution, ScaleLine } from 'ol/control';
import { Coordinate } from 'ol/coordinate';
import { DragRotate, DoubleClickZoom, DragPan, PinchRotate, PinchZoom, KeyboardPan, KeyboardZoom, MouseWheelZoom, DragZoom, Modify, Snap } from 'ol/interaction';
import { unByKey } from 'ol/Observable';
import { Extent as olExtent, getWidth, getTopLeft } from 'ol/extent';
import { getArea, getLength, getDistance } from 'ol/sphere';
import { getTransform, get as getProjection, fromLonLat } from 'ol/proj';
import IconAnchorUnits from 'ol/style/IconAnchorUnits';
import GeoJSON from 'ol/format/GeoJSON';
import { METERS_PER_UNIT } from 'ol/proj/Units';
import MapBrowserEvent from 'ol/MapBrowserEvent';
import TileGrid from 'ol/tilegrid/TileGrid';
import TileImage from 'ol/source/TileImage';
import { Size } from 'ol/size';
import Geometry from 'ol/geom/Geometry';
import { MapEvent } from 'ol';

export type OlMap = Map;
export type OlCoordinate = Coordinate;
export type OlSize = Size;
export type OlView = View;
export type OlSource = Source;
export type OlOSMSource = OSMSource;
export type OlTileDebugSource = TileDebugSource;
export type OlVectorSource = VectorSource;
export type OlXYZSource = XYZSource;
export type OlTileWMSSource = TileWMSSource;
export type OlImageWMSSource = ImageWMSSource;
export type OlBaseLayer = BaseLayer;
export type OlTileLayer = TileLayer;
export type OlVectorLayer = VectorLayer;
export type OlImageLayer = ImageLayer;
export type OlGeometry = Geometry;
export type OlFeature = Feature;
export type OlOverlay = Overlay;
export type OlInteraction = Interaction;
export type OlCircleStyle = CircleStyle;
export type OlFill = Fill;
export type OlStroke = Stroke;
export type OlStyle = Style;
export type OlGeomLineString = LineString;
export type OlGeomPolygon = Polygon;
export type OlGeomCircle = Circle;
export type OlGeomPoint = Point;
export type OlGeomMultiPoint = MultiPoint;
export type OlGeometryType = GeometryType;
export type OlDraw = Draw;
export type OlExtent = olExtent;
export type OlBaseEvent = BaseEvent;
export type OlMapEvent = MapEvent;
export type OlMapBrowserEvent = MapBrowserEvent;

export const ol = {
    Map: Map,
    View: View,
    Source: Source,
    source: {
        OSM: OSMSource,
        TileDebug: TileDebugSource,
        Vector: VectorSource,
        XYZ: XYZSource,
        TileImage: TileImage,
        TileWMS: TileWMSSource,
        WMTS: WMTSSource,
        ImageWMS: ImageWMSSource,
    },
    format: {
        GeoJSON: GeoJSON,
    },
    tilegrid: {
        TileGrid: TileGrid,
        WMTS: WMTSTileGrid
    },
    Layer: Layer,
    layer: {
        Base: BaseLayer,
        Tile: TileLayer,
        Vector: VectorLayer,
        Image: ImageLayer,
    },
    Feature: Feature,
    Overlay: Overlay,
    Interaction: Interaction,
    style: {
        Circle: CircleStyle,
        Fill: Fill,
        Stroke: Stroke,
        Icon: Icon,
        Text: Text,
        Style: Style,
    },
    geom: {
        LineString: LineString,
        Polygon: Polygon,
        Circle: Circle,
        Point: Point,
        MultiPoint: MultiPoint,
    },
    control: {
        Zoom: Zoom,
        Rotate: Rotate,
        Attribution: Attribution,
        ScaleLine: ScaleLine,
    },
    interaction: {
        DragRotate: DragRotate,
        DoubleClickZoom: DoubleClickZoom,
        DragPan: DragPan,
        PinchRotate: PinchRotate,
        PinchZoom: PinchZoom,
        KeyboardPan: KeyboardPan,
        KeyboardZoom: KeyboardZoom,
        MouseWheelZoom: MouseWheelZoom,
        DragZoom: DragZoom,
        Modify: Modify,
        Draw: Draw,
        Snap: Snap,
    },
    proj: {
        getTransform: getTransform,
        getProjection: getProjection,
        METERS_PER_UNIT: METERS_PER_UNIT,
    },
    extent: {
        getWidth: getWidth,
        getTopLeft: getTopLeft,
    },
    sphere: {
        getArea: getArea,
        getLength: getLength,
        getDistance: getDistance
    },
    enum: {
        GeometryType: GeometryType,
        OverlayPositioning: OverlayPositioning,
        IconAnchorUnits: IconAnchorUnits,
    },
    func: {
        unByKey: unByKey,
        getLayerByName: getLayerByName,
        fromLonLat: fromLonLat,
    },
}

function getLayerByName(map: OlMap, name: string) {
    let layers = map.getLayers().getArray();
    for (let i = 0; i < layers.length; i++) {
        if (name == layers[i].getProperties().name) {
            return layers[i];
        }
    }
    return null;
}