class SharedService {
    constructor() {
        let subjects = new Map();
        this.sub = (topic) => {
            if (subjects.get(topic) == null) {
                subjects.set(topic, new rxjs.Subject());
            }
            return subjects.get(topic).asObservable();
        };
        this.pub = (topic, msg) => {
            if (subjects.get(topic) == null) {
                subjects.set(topic, new rxjs.Subject());
            }
            subjects.get(topic).next(msg);
        };
        this.destroy = (topic) => {
            if (subjects.get(topic) == null) {
                subjects.delete(topic);
            }
        };
        this.destroyAll = () => {
            subjects.clear();
        };
    }
}
