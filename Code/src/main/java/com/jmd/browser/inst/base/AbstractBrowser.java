package com.jmd.browser.inst.base;

import java.awt.Component;

import com.jmd.callback.CommonAsyncCallback;
import com.jmd.callback.JavaScriptExecutionCallback;

public abstract class AbstractBrowser {

	public void create(String url, CommonAsyncCallback callback) {

	}

	public Object getBrowser() {
		return null;
	}

	public Component getContainer() {
		return null;
	}

	public String getVersion() {
		return "";
	}

	public void reload() {

	}

	public void loadURL(String url) {

	}

	public void dispose(int a) {

	}

	public void clearLocalStorage() {

	}

	public void sendShared(String topic, String message) {

	}

	public void execJS(String javaScript) {

	}

	public void execJSWithStringBack(String javaScript, JavaScriptExecutionCallback callback) {

	}

}
