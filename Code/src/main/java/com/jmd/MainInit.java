package com.jmd;

import javax.swing.SwingUtilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.jmd.common.Setting;
import com.jmd.ui.MainFrame;
import com.jmd.ui.StartupWindow;
import com.jmd.util.CommonUtils;
import com.jmd.z0test.TestFunc;

@Component
@Order(1)
public class MainInit implements ApplicationRunner {

	private static final boolean isWindows10 = CommonUtils.isWindows10();

	@Autowired
	private ApplicationTheme applicationTheme;
	@Autowired
	private MainFrame mainFrame;
	@Autowired
	private TestFunc test;

	@Override
	public void run(ApplicationArguments args) {
		SwingUtilities.invokeLater(() -> {
			if (isWindows10) {
				// Win10系统直接显示界面
				showMainFrame();
			} else {
				// 其他系统先通过jtattoo触发窗口装饰后，再回归所选主题，然后显示界面
				Setting setting = ApplicationSetting.getSetting();
				applicationTheme.change(setting.getThemeName(), setting.getThemeClazz(), () -> {
					showMainFrame();
				});
			}
		});
		new Thread(() -> {
			test.run();
		}).start();
	}
	
	private void showMainFrame() {
		mainFrame.setVisible(true);
		StartupWindow.getIstance().close();
	}

}
