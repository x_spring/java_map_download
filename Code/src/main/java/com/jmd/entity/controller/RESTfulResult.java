package com.jmd.entity.controller;

import lombok.Data;

@Data
public class RESTfulResult {

	private Integer code;
	private Boolean success;
	private String message;

}
